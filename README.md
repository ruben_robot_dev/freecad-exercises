# FreeCAD exercises

## Exercise 1:

![ex1](./static/ex-01-01.png "This the exercise 1")

Solution:
A.)

![ex1](./static/ex-01-01-A.png "This the exercise 1")

B.)

![ex1](./static/ex-01-01-B.png "This the exercise 1")

![ex1](./static/ex-01-01-B-Sketch.png "This the exercise 1")

## Exercise 2:

![ex2](./static/ex-01-02.png "This the exercise 2")

Solution:
A.)

![ex2](./static/ex-01-02-A.png "This the exercise 2")

B.)

![ex2](./static/ex-01-02-B.png "This the exercise 2")

![ex2](./static/ex-01-02-B-Sketch.png "This the exercise 2")

## Exercise 3:

![ex3](./static/ex-01-03.png "This the exercise 3")

Solution:
 A.)

![ex3](./static/ex-01-03-A.png "This the exercise 3")

B.)

![ex3](./static/ex-01-03-B.png "This the exercise 3")

![ex2](./static/ex-01-03-B-Sketch.png "This the exercise 3")

## Exercise 4:

![ex4](./static/ex-01-04.png "This the exercise 4")

Solution:
 A.)

![ex4](./static/ex-01-04-A.png "This the exercise 4")

B.)

![ex4](./static/ex-01-04-B.png "This the exercise 4")

![ex4](./static/ex-01-04-B-Sketch.png "This the exercise 4")

## Exercise 5:

![ex5](./static/ex-01-05.png "This the exercise 5")

Solution:
 A.)

![ex5](./static/ex-01-05-A.png "This the exercise 5")

B.)

![ex5](./static/ex-01-05-B.png "This the exercise 5")

![ex5](./static/ex-01-05-B-Sketch.png "This the exercise 5")

## Exercise 6:

![ex6](./static/ex-01-06.png "This the exercise 6")

Solution:
 A.)

![ex6](./static/ex-01-06-A.png "This the exercise 6")

B.)

![ex6](./static/ex-01-06-B.png "This the exercise 6")

![ex6](./static/ex-01-06-B-Sketch.png "This the exercise 6")
